import time
import RPi.GPIO as GPIO
import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import adafruit_ads1x15.differential as ads1x15
import busio
import board
import multiprocessing as mp
import socket

# initialization variables
style.use('fivethirtyeight')
fig = plt.figure()
ax2 = fig.add_subplot(2, 1, 1)
ax1 = fig.add_subplot(2, 1, 1)
ppm2 = fig.add_subplot(2, 2, 3)
ppm1 = fig.add_subplot(2, 2, 4)
x2 = []
y2 = []
xs = []
ys = []
xppm2 = []
yppm2 = []
xppm = []
yppm = []
value2 = 0
analogInputVoltage2 = 0
ppm_other = 0
value = 0
analogInputVoltage = 0
ppm = 0
IP = 0


def get_ip_address():
    ip_address = ''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address


def data():  # retrieves data from I2C 16bit ADC
    x1 = 0
    while True:
        try:
            i2c = busio.I2C(board.SCL, board.SDA)
            adc = ads1x15.ADS1115(i2c)
            GAIN = 1

            global value
            global value2
            value = adc.read_adc_difference(0, gain=GAIN)  # ppm
            value2 = adc.read_adc_difference(3, gain=GAIN)  # ppb(ppm2)
            x1 = x1 + 1
            print('ppm:', str(value) + ',' + str(x1))
            print('ppm2:', str(value2) + ',' + str(x1))
            time.sleep(0.5)
            return value
        except ValueError:
            print('ADC not found')
            pass


def DAC():  # Converts 16 bit digital signal back to analog voltage that is easier to display
    global value
    global analogInputVoltage
    global value2
    global analogInputVoltage2
    bits = 16
    ReferenceVoltage = 3.3
    DigitalOutput2 = value2
    DigitalOutput = value

    analogInputVoltage = (DigitalOutput * ReferenceVoltage) / 2 ** bits
    analogInputVoltage2 = (DigitalOutput2 * ReferenceVoltage) / 2 ** bits
    print("PPM Sensor Voltage(V): " + str(analogInputVoltage))
    print("PPM2 Sensor Voltage(V): " + str(analogInputVoltage2))
    # parts per million
    if analogInputVoltage - 0.055 >= 0:
        global ppm
        ppm = (analogInputVoltage - 0.055) / 0.0007
        print("PPM:" + "%.2f" % ppm)
    else:
        print("PPM:0")
        ppm = 0
        pass
    # parts per million 2
    if analogInputVoltage2 - 0.055 >= 0:
        global ppm_other
        ppm_other = ((analogInputVoltage2 - 0.055) / 0.0007)
        print("PPM2:" + "%.2f" % ppm_other)
    else:
        print("PPM2:0")
        ppm_other = 0
        pass
    pass


def anima(i, xs, ys, x2, y2):  # framework for live updating graphs
    # Initialization of Data collection
    #p3 = threading.Thread(target=data)
    p3 = mp.Process(target=data)
    #p4 = threading.Thread(target=DAC)
    p4 = mp.Process(target=DAC)
    p3.run()
    p4.run()
    # Open and appends to CSV file
    file = open(filename, 'a')
    now = datetime.datetime.now()
    xs.append(now)
    # Analog Voltage conversion ppm
    ys.append(analogInputVoltage)
    # Digital Output ppm
    # ys.append(value)
    x2.append(now)
    y2.append(analogInputVoltage2)
    xppm2.append(now)
    yppm2.append(eval("%.2f" % ppm_other))
    xppm.append(now)
    yppm.append(eval("%.2f" % ppm))

    # Plotter Configuration
    ax1.clear()
    ax2.clear()
    ppm2.clear()
    ppm1.clear()
    ppm2.set_title('PPM2: ' + "{0:.1f}".format(ppm_other))
    ppm1.set_title('PPM: ' + "{0:.1f}".format(ppm))
    ax1.set_xlabel('Date/Time')
    ax1.set_ylabel('Response Voltage (V)')
    ppm2.set_xlabel('Date/Time')
    ppm2.set_ylabel('PPM2')
    ppm1.set_xlabel('Date/Time')
    ppm1.set_ylabel('PPM')
    ax1.plot(xs, ys)
    try:
        global IP
        IP = get_ip_address()
    except OSError:
        IP = IP
    ax1.set_title(
        'IP:' +
        IP +
        '\n' +
        'PPM2 Sensor Voltage: ' +
        "{0:.4f}".format(analogInputVoltage2) +
        ' Volts' +
        '\n' +
        'PPM Sensor Voltage:' +
        "{0:.4f}".format(analogInputVoltage) +
        ' Volts')
    ppm2.plot(xppm2, yppm2)
    ppm1.plot(xppm, yppm)
    ax2.plot(x2, y2)

    # Writes to file
    file.write(
        str(now) +
        ',' +
        str(value) +
        "," +
        str(analogInputVoltage) +
        ',' +
        str(ppm) +
        ',' +
        str(value2) +
        "," +
        str(analogInputVoltage2) +
        ',' +
        str(ppm_other) +
        '\n')
    file.close()
    # For Future Valve control
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    # relay controls
    # GPIO.setup(4, GPIO.OUT)  # 4 valve 1 open
    # GPIO.setup(17, GPIO.OUT)  # 17 valve 1 close
    # GPIO.setup(22, GPIO.OUT)  # 22 valve 2 open


# GPIO.setup(27, GPIO.OUT)  # 27 valve 2 close

def start():  # creates new datalog on startup
    global filename
    filename = '/home/pi/Desktop/Ellington/PID logs/' + \
        input("Enter name of log file: ") + '.csv'
    file = open(filename, 'a')
    file.write(
        "Date/Time" +
        ',' +
        'PPM ADC Bit Value (16-bit)' +
        "," +
        'PPM Analog Voltage(V)' +
        ',' +
        'PPM:' +
        ',' +
        'PPM2 ADC Bit Value (16-bit)' +
        "," +
        'PPM2 Analog Voltage(V)' +
        ',' +
        'PPM2:' +
        '\n')


start()
ani = animation.FuncAnimation(fig, anima, fargs=(xs, ys, x2, y2), interval=500)
plt.show()
